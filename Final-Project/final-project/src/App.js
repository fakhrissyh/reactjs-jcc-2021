import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { toast } from "react-toastify";
import Routes from "./Routes/Routes";
import { NavProvider } from "./Context/Navcontext";
import { AuthProvider } from "./Context/Authcontext";


class App extends React.Component {
  render() {
    toast.configure();
    return (
      <div style={{backgroundColor: "#eff7cf"}} >
      <Router >
        <AuthProvider>
          <NavProvider>
            <Routes />
          </NavProvider>
        </AuthProvider>
      </Router>
      </div>
    );
  }
}

export default App;
