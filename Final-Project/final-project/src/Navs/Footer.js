import React from "react";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import { useStyles } from "./UseStyles.js";
import { Toolbar } from "@material-ui/core";

export default function Footer(props) {
    const classes = useStyles();
  
    return (
      <footer className={classes.footer} style={{backgroundColor: "#073042", height:"20%", paddingTop:"10px"}}>
        <Container maxWidth="xl">
          <Typography variant="body2" color="textSecondary" align="right">
          <div style={{display: "inline-block"}}>
            <div style={{color: "#3ddc84"}}>
              {"Jabar Coding Camp "}
            </div>
            <div style={{color: "#fff"}}>
              {new Date().getFullYear()}
            </div>
          </div>
          </Typography>
        </Container>
      </footer>
    );
  }
  