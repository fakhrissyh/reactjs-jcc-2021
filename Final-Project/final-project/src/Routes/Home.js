import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Movies from "../Contents/Movies/Movies";
import Games from "../Contents/Games/Games";

export const useStyles = makeStyles((theme) => ({
  root: {
    height: "100%",
  },
}));

const Home = (props) => {
  const classes = useStyles();

  return (
    <>
      <h1>Browse our latest games and movies</h1>
      <Movies/>
      <br/>
      <Games/>
    </>
  );
};

export default Home;
