import React, { useState } from 'react'

const Tugas11 = () => {
    const [ pesertaLomba, setPesertaLomba ] = useState([
        {nama: "Nanas", categ:"action", desc:"desc", relYear: 2000, size: 4000, price: 23, rating: 4},
        {nama: "Manggis", categ:"action", desc:"desc", relYear: 2001, size: 10000, price: 23, rating: 4},
        {nama: "Nangka", categ:"action", desc:"desc", relYear: 2010, size: 2000, price: 23, rating: 4},
        {nama: "Durian", categ:"action", desc:"desc", relYear: 2008, size: 5000, price: 23, rating: 4},
        {nama: "Strawberry", categ:"action", desc:"desc", relYear: 2014, size: 6000, price: 23, rating: 4}
      ])
    const [nama, setInputName] = useState()
    const [categ, setInputCateg] = useState()
    const [desc, setInputDesc] = useState()
    const [relYear, setInputNameRelYearhandleChangeRelYear] = useState()
    const [size, setInputNameSizehandleChangeSize] = useState()
    const [price, setInputPrice] = useState()
    const [rating, setInputRating] = useState()
    


    const [currentIndex, setCurrentIndex] = useState(-1)

    const handleChange = (event) => {
        let input = event.target.value
        setInputName(input)
    }

    const handleChangeCateg = (event) => {
        let input = event.target.value
        setInputCateg(input)
    }
    const handleChangeDesc = (event) => {
        let input = event.target.value
        setInputDesc(input)
    }
    const handleChangeRelYear = (event) => {
        let input = event.target.value
        setInputNameRelYearhandleChangeRelYear(input)
    }

    const handleChangeSize = (event) => {
        let input = event.target.value
        setInputNameSizehandleChangeSize(input)
    }
    const handleChangePrice = (event) => {
        let input = event.target.value
        setInputPrice(input)
    }

    const handleChangeRating = (event) => {
        let input = event.target.value
        setInputRating(input)
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        let newData = pesertaLomba

        if (currentIndex === -1) {
            newData = [...pesertaLomba, {nama, categ, desc, relYear, size, price, rating}]
            //console.log(newData)

        } else {
            newData[currentIndex] = {nama, categ, desc, relYear, size, price, rating}
        }
        
        if (nama !== "") {
            setPesertaLomba(newData)
            setInputName("")
        }

        if (nama !== "") {
            setPesertaLomba(newData)
            setInputCateg("")
        }

        if (nama !== "") {
            setPesertaLomba(newData)
            setInputDesc("")
        }

        if (nama !== "") {
            setPesertaLomba(newData)
            setInputPrice("")
        }

        if (nama !== "") {
            setPesertaLomba(newData)
            setInputRating("")
        }

        if (relYear !== "") {
            setPesertaLomba(newData)
            setInputNameRelYearhandleChangeRelYear("")
        }

        if (size !== "") {
            setPesertaLomba(newData)
            setInputNameSizehandleChangeSize("")
        }

    }

    const handleDelete = (event) => {
        let index = parseInt(event.target.value)
        let deletedItem = pesertaLomba[index]
        let newData = pesertaLomba.filter((e) => {return e !== deletedItem})
        setPesertaLomba(newData)
    }

    const handleEdit = (event) => {
        let index = parseInt(event.target.value)
        let editedItem = pesertaLomba[index]
        setInputName(editedItem)
        setCurrentIndex(index)
    }

    return (
        <>
            <h1>Mobile Apps List</h1>
            <table >
                <thead>
                    <tr >
                        <th>No</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Description</th>
                        <th>Release Year</th>
                        <th>Size</th>
                        <th>Price</th>
                        <th>Rating</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        pesertaLomba.map((e, index) =>  {
                            return (  
                                <tr key={index}>
                                    <td>{index + 1}</td>
                                    <td>{e.nama}</td>
                                    <td>{e.nama}</td>
                                    <td>{e.desc}</td>
                                    <td>{e.relYear}</td>
                                    <td>{e.size} MB</td>
                                    <td>{e.price}</td>
                                    <td>{e.rating}</td>
                                    <td>
                                        <button onClick={handleEdit} value={index}>update</button>
                                        <button onClick={handleDelete} value={index}>delete</button>
                                    </td>
                                </tr>                                
                            )
                        })
                    }
                </tbody>
            </table>
            <br/>
            <br/>
            <h1>Mobile Apps Form</h1>
            <form method="post" onSubmit={handleSubmit}>
                <label>Name:</label>
                <input name="name" onChange={handleChange} value={nama} type="text"/><br/>
                <label>Category:</label>
                <input name="name" onChange={handleChangeCateg} value={categ} type="text"/><br/>
                <label>Description:</label>
                <input name="name" onChange={handleChangeDesc} value={desc} type="text"/><br/>
                <label>Release Year</label>
                <input name="name" onChange={handleChangeRelYear} value={relYear} type="text"/><br/>
                <label>Size</label>
                <input name="name" onChange={handleChangeSize} value={size} type="text"/><br/>
                <label>Price</label>
                <input name="name" onChange={handleChangePrice} value={price} type="text"/><br/>
                <label>Rating</label>
                <input name="name" onChange={handleChangeRating} value={rating} type="text"/><br/>
                <input type="submit"/>
            </form>
        </>
    )
}

export default Tugas11