import axios from "axios"
import React, { useContext, useEffect, useState } from "react"
import { Link, useHistory } from "react-router-dom"
import { MobileAppContext } from "../Context/MobileAppContext"

const MobileList = () => {

   let history = useHistory()
    const { data, setData, fetchStatus, setFetchStatus, currentId, setCurrentId, input, setInput, functions} = useContext(MobileAppContext)
    const {fetchData, functionDelete} = functions

    useEffect(() => {

        if (fetchStatus) {
            fetchData()
            setFetchStatus(false)
        }


    }, [fetchStatus, setFetchStatus])

    // console.log(data)

    const handleText = (text, max) => {
        if (text === null) {
            return ""
        } else {
            return text.slice(0, max) + "..."
        }
    }


    const handleEdit = (e) => {
        let idMobile = parseInt(e.target.value)
        history.push(`/mobile-form/edit/${idMobile}`)
    
    }

    const handleDelete = (e) => {
        let idMobile = parseInt(e.target.value)

        functionDelete(idMobile)
    }



    return (
        <>
        
            <h1 style={{ padding: "30px" }}>List Mobile App</h1>
            <Link to="/mobile-form"><button>Create Data</button></Link>
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Description</th>
                        <th>Year</th>
                        <th>Size</th>
                        <th>Price</th>
                        <th>Rating</th>
                        <th>Image_url</th>
                        <th>is_android_app</th>
                        <th>is_ios_app</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {data !== null && (
                        <>
                            {data.map((e, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{e.name}</td>
                                        <td>{e.category}</td>
                                        <td>{handleText(e.description, 20)}</td>
                                        <td>{e.release_year}</td>
                                        <td>{e.size}</td>
                                        <td>{e.price}</td>
                                        <td>{e.rating}</td>
                                        <td>{handleText(e.image_url, 20)}</td>
                                        <td>{e.is_android_app === 0 ? "false" : "true"}</td>
                                        <td>{e.is_ios_app === 0 ? "false" : "true"}</td>
                                        <td>
                                            <button onClick={handleEdit} value={e.id}>edit</button>
                                            <button onClick={handleDelete} value={e.id}>delete</button>
                                        </td>
                                    </tr>
                                )
                            })}
                        </>
                    )}
                </tbody>
            </table>
            <br />
            
        </>
    )
}

export default MobileList
