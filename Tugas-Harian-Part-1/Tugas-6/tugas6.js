// Soal 1

// Luas Lingkaran
const luasLingkaran = (jejari) => {
  return 22/7 * jejari ** 2;
}

console.log(luasLingkaran(7)) // 153.86

// Keliling Lingkaran
const kelilingLingkaran = (jejari) => {
  return 22/7 * jejari * 2;
}

console.log(kelilingLingkaran(7)) // 43.96



// Soal 2
const introduce = (...dataDiri) => {
  let [nama, usia, jenisKelamin, kesibukan] = dataDiri;
  return `Pak ${nama} adalah seorang ${kesibukan} yang berusia ${usia} tahun`;
}

const perkenalan = introduce("John", "30", "Laki-Laki", "penulis")
console.log(perkenalan) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"



// Soal 3
const newFunction = function literal(firstName, lastName){
  return {
    firstName,
    lastName,
    fullName() {
      console.log(firstName + " " + lastName)
    }
  }
}

// kode di bawah ini jangan diubah atau dihapus sama sekali
console.log(newFunction("John", "Doe").firstName)
console.log(newFunction("Richard", "Roe").lastName)
newFunction("William", "Imoh").fullName()



// Soal 4
let phone = {
   name: "Galaxy Note 20",
   brand: "Samsung",
   year: 2020,
   colors: ["Mystic Bronze", "Mystic White", "Mystic Black"]
}
// kode diatas ini jangan di rubah atau di hapus sama sekali

const {name: phoneName, brand: phoneBrand, year: year} = phone; 
const [colorBronze, colorWhite, colorBlack] = phone.colors; 

// kode di bawah ini jangan dirubah atau dihapus
console.log(phoneBrand, phoneName, year, colorBlack, colorBronze);



// Soal 5
let warna = ["biru", "merah", "kuning" , "hijau"]

let dataBukuTambahan= {
  penulis: "john doe",
  tahunTerbit: 2020 
}

let buku = {
  nama: "pemograman dasar",
  jumlahHalaman: 172,
  warnaSampul:["hitam"]
}
// kode diatas ini jangan di rubah atau di hapus sama sekali

buku.warnaSampul = [...buku.warnaSampul, ...warna];
let bukuBaru  =  {...buku, ...dataBukuTambahan};
console.log(bukuBaru);




