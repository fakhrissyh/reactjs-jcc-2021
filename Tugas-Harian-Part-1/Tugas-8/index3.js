var filterBooksPromise = require('./promise2.js')

const colored40 = () => {
  filterBooksPromise(true, 40)
    .then((fulfilled) => {
            console.log(fulfilled);
        })
        .catch((error) => {
            console.log(error.message);
        });
}

colored40()

async function nonColored250(){
  console.log(await filterBooksPromise(false, 250))
}

nonColored250()

async function dataBooks() {
	try {
		var data = await filterBooksPromise(true, 30);
		console.log(data);
	} catch (item) {
		console.log(item.message);
	}
}

dataBooks();