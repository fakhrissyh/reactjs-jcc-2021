var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

var x = 0;
var eksekusi = (time, x) => {
    readBooksPromise(time, books[x])
        .then((sisaWaktu) => {
            x  ++;
            if (x < 3) {
                eksekusi(sisaWaktu, x);
            }
        })
        .catch((err) => {
            console.log(err.message);
        })
}
eksekusi(10000, 0);