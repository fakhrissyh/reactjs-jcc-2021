// Soal 1

// Perulangan pertama
var i = 2;
console.log("LOOPING PERTAMA");
while (i <= 20) {
  console.log(i  + " - I love coding");
  i += 2;
}
// Perulangan kedua
var i = 20;
console.log("LOOPING KEDUA");
while (i >= 2) {
  console.log(i  + " - I will become a frontend developer");
  i -= 2;
}



// Soal 2
for (var i = 1; i <= 20; i ++) {
  if (i % 2 != 0 && i % 3 == 0) {
  	console.log(i + ' - I Love Coding');
  } else if (i % 2 == 0) {
	console.log(i + ' - Berkualitas');
  } else if (i % 2 != 0) {
	console.log(i + ' - Santai');
  }
}



// Soal 3
var tagar = "";
var jumlah = 1
while (jumlah <= 7) {
 console.log(tagar += "#");
 jumlah ++;
}



// Soal 4
var kalimat = ["aku", "saya", "sangat", "sangat", "senang", "belajar", "javascript"];
var kalimat_baru = [];
kalimat.shift();
for (var i = 0; i < kalimat.length; i ++) {
  if (kalimat[i+1] != kalimat[i]) {
    kalimat_baru.push(kalimat[i]);
  }
}
console.log(kalimat_baru.join(" ")); // "saya sangat senang belajar javascript"



// Soal 5
var sayuran = [];
sayuran.push("Kangkung", "Bayam", "Buncis", "Kubis", "Timun", "Seledri", "Tauge");
sayuran.sort();
for (var i = 0; i < sayuran.length; i++) {
  console.log(i + 1 + ". " + sayuran[i]);
}

