// Soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var kalimat = kataPertama.concat(' ' + kataKedua.charAt(0).toUpperCase() + kataKedua.slice(1));
var kalimat = kalimat.concat(' ' + kataKetiga.slice(0,kataKetiga.length  - 1) + kataKetiga.charAt(kataKetiga.length - 1).toUpperCase())
var kalimat = kalimat.concat(' ' + kataKeempat.toUpperCase())

console.log(kalimat); // saya Senang belajaR JAVASCRIPT


// Soal 2
var panjangPersegiPanjang = "8";
var lebarPersegiPanjang = "5";

var alasSegitiga= "6";
var tinggiSegitiga = "7";

var kelilingPersegiPanjang = Number(panjangPersegiPanjang) * 2 + Number(lebarPersegiPanjang) * 2;
var luasSegitiga = Number(alasSegitiga) * Number(tinggiSegitiga) / 2;

console.log(kelilingPersegiPanjang) // 26
console.log(luasSegitiga); // 21


// Soal 3
var sentences= 'wah javascript itu keren sekali'; 

var firstWord = sentences.substring(0, 3); 
var secondWord = sentences.substring(4, 14); 
var thirdWord = sentences.substring(15, 18); 
var fourthWord = sentences.substring(19, 24); 
var fifthWord = sentences.substring(25); 

console.log('Kata Pertama: ' + firstWord); // Kata Pertama: wah
console.log('Kata Kedua: ' + secondWord); // Kata Kedua: javascript
console.log('Kata Ketiga: ' + thirdWord); // Kata Ketiga: itu
console.log('Kata Keempat: ' + fourthWord); // Kata Keempat: keren
console.log('Kata Kelima: ' + fifthWord); // Kata Kelima: sekali


// Soal 4
var nilaiJohn = 80; // Indeks John adalah A
var nilaiDoe = 50; // Indeks Doe adalah D


if (nilaiJohn >= 80) {
	console.log("Indeks John adalah A")
} else if (nilaiJohn >= 70 && nilaiJohn < 80) {
	console.log("Indeks John adalah B")
} else if (nilaiJohn >= 60 && nilaiJohn < 70) {
	console.log("Indeks John adalah C")
} else if (nilaiJohn >= 50 && nilaiJohn < 60) {
	console.log("Indeks John adalah D")
} else if (nilaiJohn < 50) {
	console.log("Indeks John adalah E")
}

if (nilaiDoe >= 80) {
	console.log("Indeks Doe adalah A")
} else if (nilaiDoe >= 70 && nilaiDoe < 80) {
	console.log("Indeks Doe adalah B")
} else if (nilaiDoe >= 60 && nilaiDoe < 70) {
	console.log("Indeks Doe adalah C")
} else if (nilaiDoe >= 50 && nilaiDoe < 60) {
	console.log("Indeks Doe adalah D")
} else if (nilaiDoe < 50) {
	console.log("Indeks Doe adalah E")
}


// Soal 5
var tanggal = 14;
var bulan = 1;
var tahun = 2002;

switch (bulan) {
	case 1:
	console.log(tanggal + " " + "Januari" + " " + tahun); // 14 Januari 2002
	break;
	case 2:
	console.log(tanggal + " " + "Februari" + " " + tahun);
	break;
	case 3:
	console.log(tanggal + " " + "Maret" + " " + tahun);
	break;
	case 4:
	console.log(tanggal + " " + "April" + " " + tahun);
	break;
	case 5:
	console.log(tanggal + " " + "Mei" + " " + tahun);
	break;
	case 6:
	console.log(tanggal + " " + "Juni" + " " + tahun);
	break;
	case 7:
	console.log(tanggal + " " + "Juli" + " " + tahun);
	break;
	case 8:
	console.log(tanggal + " " + "Agustus" + " " + tahun);
	break;
	case 9:
	console.log(tanggal + " " + "September" + " " + tahun);
	break;
	case 10:
	console.log(tanggal + " " + "Oktober" + " " + tahun);
	break;
	case 11:
	console.log(tanggal + " " + "November" + " " + tahun);
	break;
	case 12:
	console.log(tanggal + " " + "Desember" + " " + tahun);
	break;
}
