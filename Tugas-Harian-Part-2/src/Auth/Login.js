import Password from "antd/lib/input/Password";
import axios from "axios";
import Cookies from "js-cookie";
import React, { useState } from "react";
import { useHistory } from "react-router";
import { useContext } from "react/cjs/react.development";
import { UserContext } from "../context/userContext";

const Login = () => {

    let history = useHistory()

    const { userLists, setUserLists, loginStatus, setLoginStatus } = useContext(UserContext)

    const [input, setInput] = useState({
        email: "",
        password: ""
    })

    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name

        setInput({...input, [name]: value})
    }

    const handleSubmit = (event) => {
        event.preventDefault()

        // let authData = userLists.find(x =>  x.name === input.name && x.password === input.password)
        
        axios.post('https://backendexample.sanbersy.com/api/user-login',{
            email: input.email,
            password: input.password
        }).then((res) => {
            let token = res.data.token
            let user = res.data.user

            Cookies.set('token', token, {expires:  1})
            Cookies.set('name', user.name, {expires:  1})
            Cookies.set('email', user.email, {expires:  1})
            history.push('/')
        })

        // if (authData){
        //     setLoginStatus(true)
        //     Cookies.set('username', authData.username, { expires: 1 })
        //     Cookies.set('password', authData.password, { expires: 1 })
        //     history.push('/')
        //   }else{
        //     alert("username dan password gagal")
        // }
    }

    return (
        <>
            <form style={{width:"100%"}} onSubmit={handleSubmit}>
                <label>Surel</label>
                <br/>
                <input type="email" name="email" value={input.email} onChange={handleChange}/>
                <br/>
                <label>Kata sandi</label>
                <br/>
                <input type="password" name="password" value={input.password} onChange={handleChange}/>
                <br/>
                <input type="submit"/>
            </form>

        </>
    )
}

export default Login