import React, { useEffect, useState } from 'react'
import axios from 'axios'
import '.././assets/css/App.css'

const Tugas12 = () => {
    const [ pesertaLomba, setPesertaLomba ] = useState(['a','b','c'])
    const [inputName, setInputName] = useState("")
    const [inputCourse, setInputCourse] = useState("")
    const [inputScore, setInputScore] = useState("")

    const [currentIndex, setCurrentIndex] = useState(-1)

    const kondisi = (nilai) => {
        if (nilai >= 80) {
            return 'A'
        } else if (nilai >= 70 && nilai < 80) {
            return 'B'
        } else if (nilai >= 60 && nilai < 70) {
            return 'C'
        } else if (nilai >= 50 && nilai < 60) {
            return 'D'
        } else if (nilai < 50) {
            return 'E'
        }
    }

    useEffect(() => {
        const fetchData = async () => {
            let result = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)
            let data = result.data
            let output = data.map((e)=>{
                return {
                    id : e.id,
                    name : e.name,
                    course: e.course,
                    score: e.score
                    
                }
            })
            setPesertaLomba(output)
        }

        fetchData()
    },[])


    const handleChange = (event) => {
        let input = event.target.value
        setInputName(input)
    }

    const handleChangeCourse = (event) => {
        let input = event.target.value
        setInputCourse(input)
    }

    const handleChangeScore = (event) => {
        let input = event.target.value
        setInputScore(input)
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        if (currentIndex === -1) {
            axios.post(`http://backendexample.sanbercloud.com/api/student-scores`,{
            name: inputName,
            course: inputCourse,
            score: inputScore
            })
            .then((res) => {
                setPesertaLomba([...pesertaLomba, {
                    id:res.data.id,
                    name:res.data.name,
                    course:res.data.course,
                    score:res.data.score
                }])
            })
        } else {
            axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentIndex}`,{
                name: inputName,
                course: inputCourse,
                score: inputScore
            })
            .then((res) => {
                let updatedItem = pesertaLomba.find((e) => e.id === currentIndex)
                updatedItem.name = inputName
                updatedItem.course = inputCourse
                updatedItem.score = inputScore
                setPesertaLomba([...pesertaLomba])
            })
        }
        setInputName("")
        setInputCourse("")
        setInputScore("")
        setCurrentIndex(-1)
    }

    const handleDelete = (event) => {
        let idPeserta = parseInt(event.target.value)

        axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${idPeserta}`)
        .then(()=>{
            let newPesertaLomba = pesertaLomba.filter((e)=>{return e.id !== idPeserta})
            setPesertaLomba(newPesertaLomba)
        })
    }

    const handleEdit = (event) => {
        let idPeserta = parseInt(event.target.value)
        axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${idPeserta}`)
        .then((res) => {
            let data = res.data
            setInputName(data.name)
            setInputCourse(data.course)
            setInputScore(data.score)
            setCurrentIndex(data.id)
        })
    }
 
    return (
        <>
            <div className="container">
            <div>
                <h1>Daftar Nilai Mahasiswa</h1>
                <table >
                    <thead>
                        <tr >
                            <th>No</th>
                            <th>Nama</th>
                            <th>Mata Kuliah</th>
                            <th>Nilai</th>
                            <th>Indeks Nilai</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            pesertaLomba.map((e) =>  {
                                return (  
                                    <tr key={e.id}>
                                        <td>{e.id}</td>
                                        <td>{e.name}</td>
                                        <td>{e.course}</td>
                                        <td>{e.score}</td>
                                        <td>{kondisi(e.score)}</td>
                                        <td>
                                            <button onClick={handleEdit} value={e.id}>update</button>
                                            <button onClick={handleDelete} value={e.id}>delete</button>
                                        </td>
                                    </tr>                                
                                )
                            })
                        }
                    </tbody>
                </table>
                <br/>
                <br/>
                </div>
                <div>
                <h1>Form Nilai Mahasiswa</h1>
                <form method="post" onSubmit={handleSubmit} className="form">
                    <label>Nama</label>
                    <input name="name" onChange={handleChange} value={inputName} type="text"/><br/>
                    <label>Mata Kuliah</label>
                    <input name="name" onChange={handleChangeCourse} value={inputCourse} type="text"/><br/>
                    <label>Nilai</label>
                    <input name="name" onChange={handleChangeScore} value={inputScore} type="text"/><br/>
                    <label></label>
                    <input type="submit"/>
                </form>
                <br/>
                <br/>
                </div>
            </div>
        </>
    )
}

export default Tugas12