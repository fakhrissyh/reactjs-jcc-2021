import React, { useContext, useEffect, useState } from 'react'
import axios from 'axios'
import { PesertaLombaContext }  from '../Tugas-13 II/pesertaLombaContext'

const StudentList = () => {
    const {pesertaLomba, functions} = useContext(PesertaLombaContext)

    const {fetchData, functionDelete, functionEdit} = functions

    useEffect(() => {
        fetchData()
    },[])
 

    const kondisi = (nilai) => {
        if (nilai >= 80) {
            return 'A'
        } else if (nilai >= 70 && nilai < 80) {
            return 'B'
        } else if (nilai >= 60 && nilai < 70) {
            return 'C'
        } else if (nilai >= 50 && nilai < 60) {
            return 'D'
        } else if (nilai < 50) {
            return 'E'
        }
    }



    const handleDelete = (event) => {
        let idPeserta = parseInt(event.target.value)
        functionDelete(idPeserta)
    }

    const handleEdit = (event) => {
        let idPeserta = parseInt(event.target.value)
        functionEdit(idPeserta)
    }
 
    return (
        <>
            <div className="container">
            <div>
                <h1>Daftar Nilai Mahasiswa</h1>
                <table >
                    <thead>
                        <tr >
                            <th>No</th>
                            <th>Nama</th>
                            <th>Mata Kuliah</th>
                            <th>Nilai</th>
                            <th>Indeks Nilai</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            pesertaLomba.map((e) =>  {
                                return (  
                                    <tr key={e.id}>
                                        <td>{e.id}</td>
                                        <td>{e.name}</td>
                                        <td>{e.course}</td>
                                        <td>{e.score}</td>
                                        <td>{kondisi(e.score)}</td>
                                        <td>
                                            <button onClick={handleEdit} value={e.id}>update</button>
                                            <button onClick={handleDelete} value={e.id}>delete</button>
                                        </td>
                                    </tr>                                
                                )
                            })
                        }
                    </tbody>
                </table>
                <br/>
                <br/>
                </div>
                <div>
                <h1>Form Nilai Mahasiswa</h1>
                
                <br/>
                <br/>
                </div>
            </div>
        </>
    )
}

export default StudentList