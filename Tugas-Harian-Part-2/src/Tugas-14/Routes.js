import React from 'react'
//import './assets/css/App.css'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  Link,
  useParams
} from "react-router-dom";
import { Layout, Menu, Breadcrumb } from 'antd';
import LayoutComponent from '../layout/layout';
import About from './about';
import Tugas_11 from './Tugas-11';
import Tugas_12 from './Tugas-12';
import Login from '../Auth/Login';
import { UserProvider } from '../context/userContext';
import Cookies from 'js-cookie';
import Register from '../Auth/Register';
import StudentList from '../Component/StudentList';
import StudentForm from '../Component/StudentForm';

const { Header, Content, Footer } = Layout;



function Routes() {

  const LoginRoute = ({...props}) => {

    if (Cookies.get('token') !== undefined) {
      return <Redirect path="/" />
    } else {
      return <Route {...props} />
    }

  }

  return (
    <>
      <Router>
        
          <Layout className="layout">
            <UserProvider>
            <div>
              <Switch>
                <Route path="/about" exact>
                  <LayoutComponent content={<About />} />
                </Route>
                <Route path="/blog/:slug" exact>
                  <LayoutComponent content={<Tugas_11 />} />
                </Route>
                <Route path="/" exact>
                  <LayoutComponent content={<Tugas_12 />} />
                </Route>
                {/* <Route path='/example' exact>
                  <LayoutComponent content={<Example />} />
                </Route> */}
                <LoginRoute path="/login" exact>
                  <LayoutComponent content={<Login/>} />
                </LoginRoute>
                <LoginRoute path="/register" exact>
                  <LayoutComponent content={<Register/>} />
                </LoginRoute>
                <Route path='/example' exact>
                  <LayoutComponent content={<StudentList />} />
                </Route>
                <Route path='/example/form' exact>
                  <LayoutComponent content={<StudentForm />} />
                </Route>
                <Route path='/example/form/:Id' exact>
                  <LayoutComponent content={<StudentForm />} />
                </Route>
              </Switch>
            </div>
            </UserProvider>
          </Layout>
      
      </Router>
    </>
  );  
}

export default Routes;
