import React from "react"
// import { Layout, Menu, Breadcrumb } from 'antd';



///
import { Layout, Menu, Breadcrumb } from 'antd';
import { UserOutlined, LaptopOutlined, NotificationOutlined } from '@ant-design/icons';

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;
///



// const { Header, Content, Footer } = Layout;

const LayoutAntd = () => {
    return (
        <>
           
                <Content style={{ padding: '0 50px'}}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item>Home</Breadcrumb.Item>
                    <Breadcrumb.Item>List</Breadcrumb.Item>
                    <Breadcrumb.Item>App</Breadcrumb.Item>
                </Breadcrumb>
                <div className="site-layout-content" style={{minHeight: "100vh"}}>Content</div>
                </Content>
            
        </>
    )
}

export default LayoutAntd