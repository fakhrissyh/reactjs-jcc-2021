import React from "react"
import { Link, useHistory } from "react-router-dom"
import { Layout, Menu, Breadcrumb } from 'antd';
import { useContext } from "react/cjs/react.development";
import { UserContext } from "../context/userContext";
import Cookies from "js-cookie";

const { Header, Content, Footer } = Layout;

const Nav = () => {
    
    let history = useHistory()

    const {loginStatus, setLoginStatus} = useContext(UserContext)

    const handleLogout = () => {
        setLoginStatus(false)
        Cookies.remove('token')
        Cookies.remove('name')
        history.push('/login')
    }

    return (
        <>
        <Header>
            <div className="logo" />
            <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
                <Menu.Item key={"1"}><Link to="/about">Tugas 9</Link></Menu.Item>
                <Menu.Item key={"2"}><Link to="/blog/:slug">Tugas 11</Link></Menu.Item>
                {/* <Menu.Item key={"3"}><Link to="/antd">Layout Antd</Link></Menu.Item> */}
                <Menu.Item key={"3"}><Link to="/">Tugas 12</Link></Menu.Item>
                
                {
                    Cookies.get('token') !== undefined && <Menu.Item key={"4"}><Link to="/example">Example Token</Link></Menu.Item>
                }

                {
                    Cookies.get('token') !== undefined && <Menu.Item key={"5"} style={{position: "absolute", right:"0"}} onClick={handleLogout}><span>Logout</span></Menu.Item>
                    // {/* otentifikasi bagian 1: loginStatus !== false && <Menu.Item key={"5"} style={{position: "absolute", right:"0"}} onClick={handleLogout}><span>Logout</span></Menu.Item> */}
                }
                {
                    Cookies.get('token') === undefined && (
                        <>
                            <Menu.Item key={"4"} style={{position: "absolute", right:"80px"}}><Link to="/register">Register</Link></Menu.Item> 
                            <Menu.Item key={"5"} style={{position: "absolute", right:"0"}}><Link to="/login">Login</Link></Menu.Item>
                        </>
                    )
                    // otentifikasi bagian 1: loginStatus === false && <Menu.Item key={"4"} style={{position: "absolute", right:"0"}}><Link to="/login">Login</Link></Menu.Item> 
                }
                
            </Menu>
        </Header>
        </>
    )
}

export default Nav