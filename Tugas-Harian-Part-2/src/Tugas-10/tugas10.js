import React, { useState } from 'react';


export const App1 = () => {
    let time = new Date().toLocaleTimeString();
    const [ctime, setCtime] = useState(time);
  
    const UpdateTime = () => {
        time = new Date().toLocaleTimeString();
        setCtime(time);
    };
  
    setInterval(UpdateTime, 1000);
    
    return (
      <div>
        <h1>Now at: {ctime}</h1>
        <h3>Countdown: </h3>
      </div>
    );
  };

export default App1;