import React, { useContext } from "react";
import { MovieContext } from "./movieContext";

const MovieList = () => {
    const {  movie, setMovie } = useContext(MovieContext)
    console.log(movie)
    return (
        <>
            <ul>
                {movie.map((e)=>{
                    return <li>Name: {e.name}, {e.lengthOfTime} minutes</li>
                })}
            </ul>
        </>
    )
}

export default MovieList