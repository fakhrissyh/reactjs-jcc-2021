import React, { useContext, useState } from "react";
import { MovieContext } from "./movieContext";

const MovieForm = () => {

    const { movie, setMovie } = useContext(MovieContext)
    const [ input, setInput ] = useState({
        name:"",
        lengthOfTime: 0
    })

    const handleSubmit = (event) => {
        event.preventDefault()

        setMovie([...movie, {
            name: input.name,
            lengthOfTime: input.lengthOfTime
        }])

        setInput({
            name:"",
            lengthOfTime: 0
        })
    }

    const handleChange = (event) => {
        let typeOfValue = event.target.value
        let name = event.target.name

        setInput({...input,[name]:typeOfValue})
    }

    return (
        <form method="post" onSubmit={handleSubmit}>
            <label>Name:</label>
            <input onChange={handleChange} value={input.name} name="name" type="text"/>
            <br/>
            <label>lengthOfTime:</label>
            <input onChange={handleChange} value={input.lengthOfTime} name="lengthOfTime" type="number"/>
            <br/>
            <input type="submit"/>
        </form>
    )
}

export default MovieForm