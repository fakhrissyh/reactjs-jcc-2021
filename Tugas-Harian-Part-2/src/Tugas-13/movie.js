import React from "react";
import { MovieProvider } from "./movieContext";
import MovieForm from "./movieForm";
import MovieList from "./movieList";

const Movie = () => {
    return (
        <MovieProvider>
            <MovieList/>
            <MovieForm/>
        </MovieProvider>
    )
}

export default Movie