import React from 'react'
import { PesertaLombaProvider } from './pesertaLombaContext'
import PesertaLombaList from './pesertaLombaList'
import PesertaLombaForm from './pesertaLombaForm'

const PesertaLomba = () => {
    return (
        <PesertaLombaProvider>
            <PesertaLombaList/>
            <PesertaLombaForm/>
        </PesertaLombaProvider>
    )
}

export default PesertaLomba