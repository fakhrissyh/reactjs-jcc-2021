import React, { createContext, useState } from 'react'
import axios from 'axios'

export const PesertaLombaContext = createContext()

export const PesertaLombaProvider = props => {

    const [pesertaLomba, setPesertaLomba] = useState([])
    const [inputName, setInputName] = useState("")
    const [inputCourse, setInputCourse] = useState("")
    const [inputScore, setInputScore] = useState("")
    const [currentIndex, setCurrentIndex] = useState(-1)

    const fetchData = async () => {
        let result = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)
        let data = result.data
        let output = data.map((e)=>{
            return {
                id : e.id,
                name : e.name,
                course: e.course,
                score: e.score
                
            }
        })
        setPesertaLomba(output)
    }

    const functionDelete = (idPeserta) => {
        axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${idPeserta}`)
        .then(()=>{
            let newPesertaLomba = pesertaLomba.filter((e)=>{return e.id !== idPeserta})
            setPesertaLomba(newPesertaLomba)
        })
    }

    const functionSubmit = (params) => {
        axios.post(`http://backendexample.sanbercloud.com/api/student-scores`,{
            name: inputName,
            course: inputCourse,
            score: inputScore
            })
            .then((res) => {
                setPesertaLomba([...pesertaLomba, {
                    id:res.data.id,
                    name:res.data.name,
                    course:res.data.course,
                    score:res.data.score
                }])
            })
    }

    const functionUpdate = (params) => {
        axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentIndex}`,{
                name: inputName,
                course: inputCourse,
                score: inputScore
            })
            .then((res) => {
                let updatedItem = pesertaLomba.find((e) => e.id === currentIndex)
                updatedItem.name = inputName
                updatedItem.course = inputCourse
                updatedItem.score = inputScore
                setPesertaLomba([...pesertaLomba])
            })
    }

    const functionEdit =  (idPeserta) => {
        axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${idPeserta}`)
        .then((res) => {
            let data = res.data
            setInputName(data.name)
            setInputCourse(data.course)
            setInputScore(data.score)
            setCurrentIndex(data.id)
        })
    }

    const functions = {
        fetchData,
        functionDelete,
        functionSubmit,
        functionUpdate,
        functionEdit
    }

    return (
        <PesertaLombaContext.Provider value={{
            pesertaLomba,
            setPesertaLomba,
            inputName,
            setInputName,
            inputCourse,
            setInputCourse,
            inputScore,
            setInputScore,
            currentIndex,
            setCurrentIndex,
            functions
        }}>
            {props.children}
        </PesertaLombaContext.Provider>
    )
}