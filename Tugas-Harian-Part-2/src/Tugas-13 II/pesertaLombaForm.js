import React, { useContext } from 'react'
import { PesertaLombaContext } from './pesertaLombaContext'

const PesertaLombaForm = () => {

    const { inputName, setInputName, inputCourse, setInputCourse, inputScore, setInputScore, currentIndex, setCurrentIndex, functions } = useContext(PesertaLombaContext)

    const {functionSubmit, functionUpdate} = functions

    const handleChange = (event) => {
        let input = event.target.value
        setInputName(input)
    }

    const handleChangeCourse = (event) => {
        let input = event.target.value
        setInputCourse(input)
    }

    const handleChangeScore = (event) => {
        let input = event.target.value
        setInputScore(input)
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        if (currentIndex === -1) {
            functionSubmit()
        } else {
            functionUpdate()
        }
        setInputName("")
        setInputCourse("")
        setInputScore("")
        setCurrentIndex(-1)
    }
    return (
        <form method="post" onSubmit={handleSubmit}>
            <label>Nama</label>
            <input name="name" onChange={handleChange} value={inputName} type="text"/><br/>
            <label>Mata Kuliah</label>
            <input name="name" onChange={handleChangeCourse} value={inputCourse} type="text"/><br/>
            <label>Nilai</label>
            <input name="name" onChange={handleChangeScore} value={inputScore} type="text"/><br/>
            <label></label>
            <input type="submit"/>
        </form>
    )
}

export default PesertaLombaForm