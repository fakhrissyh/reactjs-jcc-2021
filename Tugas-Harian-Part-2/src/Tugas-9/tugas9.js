import React from 'react';
import logo from './logo-jcc.png';
import './App2.css';
import {App1} from '.././Tugas-10/tugas10';

function Welcome(props){
  return (
    <div className="div1">
      <section className="list">
        <input type="checkbox"/>
        <p className="p0">Belajar {props.name}</p>
      </section>
    </div>
  );
};


function App() {
  return (
    <div>
      
      <App1 />
      <div className="Box">
        <div className="App">
          <img src={logo} className="App-logo" />
          <p className="p1">THINGS TO DO</p>
          <p className="p2">During Bootcamp in Jabarcodingcamp</p>
          <Welcome name="GIT & CLI"/>
          <Welcome name="HTML & CSS" />
          <Welcome name="Javascript" />
          <Welcome name="ReactJS Dasar" />
          <Welcome name="ReactJS Advance" />   
          <input type="button" className="Button" value="Send"/>   
        </div>
      </div>
    </div>
  );
}

export default App;